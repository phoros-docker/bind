FROM alpine:edge
MAINTAINER Tobias Bohrmann

# Update and install needed packages
RUN apk add --update --no-cache --no-progress bash bind bind-tools su-exec \
    && mkdir -p /run/named \
    && chown named:root /run/named \
    && chmod 750 /run/named

# Copy the default configuration for BIND
COPY ./config/ /etc/bind/

# Copy docker-entrypoint.sh
COPY ./assets/docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

EXPOSE 53
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/sbin/named","-g","-c","/etc/bind/named.conf","-u","named"]
